import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import { getMessagesQuery } from '../queries/queries';

class BookList extends Component {
    constructor(props){
        super(props);
        this.state = {
            selected: null
        }
    }
    displayMessages(){
        var data = this.props.data;
        console.log(data);
        if(data.loading){
            return( <div>Loading books...</div> );
        } else {
            return data.attractions.map(msg => {
                return(
                    <li key={ msg.id } >{ msg.name }</li>
                );
            })
        }
    }
    render(){
        return(
            <div>
                <ul id="book-list">
                    { this.displayMessages() }
                </ul>
            </div>
        );
    }
}

export default graphql(getMessagesQuery)(BookList);