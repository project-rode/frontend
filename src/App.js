import React from 'react';
import { ApolloClient } from 'apollo-client';
import { HttpLink, InMemoryCache } from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import MessageList from './components/MessageList';
require('dotenv').config();

const client = new ApolloClient({
  link: new HttpLink({uri: `${process.env.REACT_APP_LOCAL ? process.env.REACT_APP_LOCAL : "http://localhost:4001"}/graphql/`}),
  cache: new InMemoryCache(),
});


const App = () => {
  return (
    <ApolloProvider client={client}>
            <div id="main">
                <h1>Ninja's Reading List</h1>
                <MessageList />
            </div>
        </ApolloProvider>
  );
}

export default App;
