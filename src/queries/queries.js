import {gql} from 'apollo-boost';

const getMessagesQuery = gql`
    {
        attractions {
            id
            name
        }
    }
`;

export {getMessagesQuery};